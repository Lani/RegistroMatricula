/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model.dao;

/**
 *
 * 
 * @author Lani Sauna
 */
import java.sql.*;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entidade.Cliente;

/**
 *
 */
public class ClienteDAO implements DAOinterface <Cliente, Integer>{
    
    private Connection conexao;
    
    public ClienteDAO (){
        BDconexao bd = new BDconexao();
        try {
            conexao = bd.getConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Cliente inserir(Cliente c) {
        String sql = "Insert into cliente(nome,matricula, sexo, idade)VALUES(?,?,?,?)";
                 
        try {
            try (PreparedStatement stmt = this.conexao.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS)) {
                stmt.setString(1, c.getNome());
                stmt.setString(3, c.getSexo());
                stmt.setInt(2, c.getMatricula());
                stmt.setInt(4, c.getIdade());
                
               stmt.execute();
                
                ResultSet rs = stmt.getGeneratedKeys();
                
                if(rs.next()){
                    c.setIdCliente(rs.getInt(1));
                }
            }
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public Cliente update(Cliente c) {
        String sql = "UPDATE Cliente SET nome=?, matricula=?, sexo=?, idade=?";
                
        try {
            try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
                stmt.setString(1, c.getNome());
                stmt.setInt(2, c.getMatricula());
                stmt.setString(3, c.getSexo());
                stmt.setInt(4, c.getIdade());
                stmt.setInt(8, c.getIdCliente());
                
                stmt.executeUpdate();
            }
            
            return c;
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }

    @Override
    public void delete(Integer i) {
        try {
            String sql= "Delete from Cliente where idCliente=?";
            PreparedStatement st=conexao.prepareStatement(sql);
            
            st.setInt(1,i);
            st.execute();
            st.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public List<Cliente> listarTodos() {
        String sql = "SELECT * FROM Cliente";
        List<Cliente> clientes = new ArrayList<>();
        try {
            PreparedStatement stmt = conexao.prepareStatement(sql);
            
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()){
                Cliente c = new Cliente();
                c.setNome(rs.getString("nome"));
                c.setMatricula(rs.getInt("matricula"));
                c.setIdade(rs.getInt("idade"));
                c.setIdCliente(rs.getInt("idCliente"));
                c.setSexo(rs.getString("sexo"));
                
                clientes.add(c);
                
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return clientes;
    }

    @Override
    public Cliente listarPorID(Integer i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
